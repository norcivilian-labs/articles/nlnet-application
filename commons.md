Dear Anton Davydov,

A proposal for a grant application was sent to NLnet Foundation (https://nlnet.nl) using this email address. Your project has received code 2024-02-161. The text of the proposal is shown below for your records. If you want to make changes to this proposal before the deadline, feel free to resubmit. We will send you a follow-up email after the deadline has passed to provide more information regarding the review process.

Important: if this request did NOT originate from you (and you are unaware of others that could have made this application on your behalf), do NOT CLICK on any links in this message nor open attachments. Please notify us if this is the case, otherwise you can expect at least two other emails from us. If that is not a problem, feel free to discard immediately.

Best regards,
the NLnet team

Code        : 2024-02-161
Requestor   : Olivier Fantazor
Email       : fetsorn@gmail.com
Phone       : +995599451290
Organization: Norcivilian Labs
Country     : France, Spain, Georgia, Armenia
Consent     : You may keep my data on record
Call        : Open_Call
Project     : evenor, csvs
Website     : https://gitlab.com/norcivilian-labs/evenor


# Abstract
TODO: code in the open
TODO: restore health of internet
TODO: relevance to next generation initiative
TODO: R&D
TODO: Resilient
TODO: Trustworthy
TODO: Sustainable
license is 

evenor is an open source program with naive plain-text storage for notes, messages, photos, documents.
user can create datasets with data, add entries, can view data in the UI, can search, filter and sort, view attached files.
user can rescue data from proprietary services, collect all personal data locally, and later upload data to social networks and public hosting for syndication.
user can share data on git hosting platforms, edit entries in collaboration with other uses, track the history of changes in each dataset.

evenor stores each dataset in a git repository that contains a csvs dataset. csvs is a plain-text relational database that consists of .csv files. more at https://csvs-lib.docs.norcivilianlabs.org
on desktop and mobile, evenor interacts with a set of git+csvs directories on the filesystem. in browser, evenor interacts with the virtual filesystem in IndexedDB and isomorphic-git, a JavaScript git impementation. For sharing and backup of data, user can push csvs datasets to git hosting services like gitlab.

# Experience
I have designed both evenor and csvs, and lead the team at Norcivilian Labs which currently maintains the projects. I write specifications, organize the workflow, mentor engineers from junior to middle level, and for several years I've built in the crossplatform/web space with focus on furthering the capabilities and sanity in web, contributing to data standards, nixpkgs, building toolchains for web assembly.

Amount      : € 5000
Use         : budget will be used to pay software engineers for reaching development milestones. 300h

Anton Davydov (fetsorn) $50/h
Nasim Abdulloev $25/h

Project never had other funding sources.
We do not purchase hardware. We host releases and run CI/CD pipeline at Gitlab at no cost.

# Comparison  : 
All popular note-taking, social media and messaging programs store data in binary databases that can only be accessed by superusers, and lack export options. Evenor prioritizes highly accessible and durable plain-text data storage, as well abundunant list of import and export options for data interchange. evenor is one of the first in the market to build on Tauri, which provides a more secure and uniform mobile/desktop UI than the Electron used in previous iterations of evenor, Notion. Evenor strives for powerful search and interaction with services, as well as free and open-source development, so it is more reliable long-term than the proprietary projects that cut and add features out of control of the user base, like onenote, iNotes.

csvs is a plain-text relational database. we worked with recutils for years before making csvs, however recutils had segfaults, no maintenance and a syntax that required excessive space. SQL databases are customary for data storage, and they provide faster indexing, but we argue that the difference in efficiency is not a deciding factor in small to medium personal datasets, but the lower level of entry to working with csvs opens the data storage to the user user and gives them back their agency. the priority of csvs is to keep the format naive, such that an engineer could write a client in a day, and even a layman could retrieve data in case of emergency or bitrot - gradually adding more powerful sql-like search features.

the main goal of current development story is going mobile, but we must also refactor existing features will also require refactor and improvement. New format of csvs introduces a proper specification, leaner architecture, and a plugin/extension system. Tauri requires a Rust csvs library, which will result in more efficient csvs processing.

Challenges  : design architecture of csvs-rs. based on former Shell and JavaScript implementations, the Rust library must implement an updated csvs format specification, and read/write the filesystem in streams.

designing a new UI overview. Formerly built around the concept of "timelines", new UI must resemble iPhone notes, to provide highly useable and familiar interface to the users

we would love to support pijul in addition to git. On dekstop and mobile, integrating pijul rust code will be straightforward, and on web it will require writing a JavaScript implemention like isogit, perhaps with a WASM module for the pijul logic. If this focus is of interest to NLNet, we'd be happy to prioritise this, as there's extensive experience with both WASM and isogit / virtual fs in browser, and the folks at pijul sure would be happy to collaborate.

Ecosystem   : currently evenor is developed by 2 engineers and 1 designer out of 8 people at Norcivilian Labs.

we have a few users that store their family histories in evenor, because it has great support for GEDCOM genealogy data and life story editing. After moving to mobile, we will market the app at genealogy communities and attract more users.

we have a couple users that syndicate content from evenor to social networks. Currently we support publishing to Telegram channels and to RSS, and then [morio](https://gitlab.com/norcivilian-labs/morio) turns RSS into weblogs. Support for more federalized social networks is down the line.

we are acquaintances with selfprivacy.org and will discuss support for compatible CORS headers on their self-hosted Gitea instances, so that users can view datasets with the web version of evenor at qua.qualifiedself.org


