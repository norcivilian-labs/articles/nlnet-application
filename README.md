Next deadline: February 1st 2024 12:00 CET (noon)

This form can be used to request support for your project. Note that releasing software, hardware and content under libre/open licenses, and the application of open standards where possible are transversal requirements for all. From our end we will provide a transparent and efficient selection process. Please check the (call-specific) guide for applicants for the call you are interested in.

Please also don't forget to look into our privacy statement about how we deal with your information (should be a pleasant surprise, actually — we really care about your privacy. Throughout the years we've funded the development of quite some widespread privacy enhancing technologies ourselves).

A practical point: we recommend to prepare longer answers offline, in case something goes wrong with your browser session. It is a light weight procedure, please don't wait until the last hour before the deadline before submitting — deadlines are hard.

Please select a call
In the list of current calls below, please indicate the call topic you are responding to. Note that some larger funds (like NGI0 Core and NGI0 Commons Fund) (part of the Next Generation Internet initiative) will have some special scope or conditions. You'd better have a look at them before you submit a proposal. If in doubt, submit to our open call and mention in the application that you are okay with us allocating your proposal to the most suitable fund.
Thematic call 	

Contact information
Your name (max. 100 characters) 	 ANTON DAVYDOV
Email address 	                     fetsorn@gmail.com
Phone number 	                     +995599451290
Organisation (max. 100 characters) 	 Norcivilian Labs (association, not incorporated)
Country                              Republic of Georgia

General project information
Project name (max. 100 characters) 	 evenor, csvs
Website / wiki 	                     https://gitlab.com/norcivilian-labs/evenor

Please be short and to the point in your answers; focus primarily on the what and how, not so much on the why. Add longer descriptions as attachments (see below). If English isn't your first language, don't worry — our reviewers don't care about spelling errors, only about great ideas. We apologise for the inconvenience of having to submit in English. On the up side, you can be as technical as you need to be (but you don't have to). Do stay concrete. Use plain text in your reply only, if you need any HTML to make your point please include this as attachment.

Abstract: Can you explain the whole project and its expected outcome(s).

1200 characters
```!
evenor is an open source program with naive plain-text storage for notes, messages, photos, documents. 
user can create datasets with data, add entries, can view data in the UI, can search, filter and sort, view attached files. 
user can rescue data from proprietary services, collect all personal data locally, and later upload data to social networks and public hosting for syndication. 
user can share data on git hosting platforms, edit entries in collaboration with other uses, track the history of changes in each dataset.

evenor stores each dataset in a git repository that contains a csvs dataset. csvs is a plain-text relational database that consists of .csv files. more at https://csvs-lib.docs.norcivilianlabs.org
on desktop and mobile, evenor interacts with a set of git+csvs directories on the filesystem. in browser, evenor interacts with the virtual filesystem in IndexedDB and isomorphic-git, a JavaScript git impementation. For sharing and backup of data, user can push csvs datasets to git hosting services like gitlab. 


```

Have you been involved with projects or organisations relevant to this project before? And if so, can you tell us a bit about your contributions?

(Optional) This can help us determine if you are the right person to undertake this effort

```!
I have designed both evenor and csvs, and lead the team at Norcivilian Labs which currently maintains the projects. I write specifications, organize the workflow, mentor engineers from junior to middle level, and for several years I've built in the crossplatform/web space with focus on furthering the capabilities and sanity in web, contributing to data standards, nixpkgs, building toolchains for web assembly.```

Requested support
Requested Amount between 5000 and 50000 (in Euro) 10000

Explain what the requested budget will be used for?

Does the project have other funding sources, both past and present?
(If you want, you can in addition attach a budget at the bottom of the form)

Explain costs for hardware, human labor (including rates used), travel cost to technical meetings, etc.

```!
budget will be used to pay software engineers for reaching development milestones. 300h
Anton Davydov (fetsorn) $50/h
Nasim Mora $25/h

Project never had other funding sources. 
We do not purchase hardware. We host releases and run CI/CD pipeline at Gitlab at no cost.
```

Compare your own project with existing or historical efforts.

(e.g. what is new, more thorough or otherwise different)

```!
All popular note-taking, social media and messaging programs store data in binary databases that can only be accessed by superusers, and lack export options. Evenor prioritizes highly accessible and durable plain-text data storage, as well abundunant list of import and export options for data interchange. evenor is one of the first in the market to build on Tauri, which provides a more secure and uniform mobile/desktop UI than the Electron used in previous iterations of evenor, Notion. Evenor strives for powerful search and interaction with services, as well as free and open-source development, so it is more reliable long-term than the proprietary projects that cut and add features out of control of the user base, like onenote, iNotes. 

csvs is a plain-text relational database. we worked with recutils for years before making csvs, however recutils had segfaults, no maintenance and a syntax that required excessive space. SQL databases are customary for data storage, and they provide faster indexing, but we argue that the difference in efficiency is not a deciding factor in small to medium personal datasets, but the lower level of entry to working with csvs opens the data storage to the user user and gives them back their agency. the priority of csvs is to keep the format naive, such that an engineer could write a client in a day, and even a layman could retrieve data in case of emergency or bitrot - gradually adding more powerful sql-like search features. 

the main goal of current development story is going mobile, but we must also refactor existing features will also require refactor and improvement. New format of csvs introduces a proper specification, leaner architecture, and a plugin/extension system. Tauri requires a Rust csvs library, which will result in more efficient csvs processing.
```

What are significant technical challenges you expect to solve during the project, if any?)

(optional but recommended)

```!
design architecture of csvs-rs. based on former Shell and JavaScript implementations, the Rust library must implement an updated csvs format specification, and read/write the filesystem in streams.

designing a new UI overview. Formerly built around the concept of "timelines", new UI must resemble iPhone notes, to provide highly useable and familiar interface to the users 

we would love to support pijul in addition to git. On dekstop and mobile, integrating pijul rust code will be straightforward, and on web it will require writing a JavaScript implemention like isogit, perhaps with a WASM module for the pijul logic. If this focus is of interest to NLNet, we'd be happy to prioritise this, as there's extensive experience with both WASM and isogit / virtual fs in browser, and the folks at pijul sure would be happy to collaborate. 
```

Describe the ecosystem of the project, and how you will engage with relevant actors and promote the outcomes?

(E.g. which actors will you involve? Who should run or deploy your solution to make it a success?)

```!
currently evenor is developed by 2 engineers and 1 designer out of 8 people at Norcivilian Labs.

we have a few users that store their family histories in evenor, because it has great support for GEDCOM genealogy data and life story editing. After moving to mobile, we will market the app at genealogy communities and attract more users.

we have a couple users that syndicate content from evenor to social networks. Currently we support publishing to Telegram channels and to RSS, and then [morio](https://gitlab.com/norcivilian-labs/morio) turns RSS into weblogs. Support for more federalized social networks is down the line.

we are acquaintances with selfprivacy.org and will discuss support for compatible CORS headers on their self-hosted Gitea instances, so that users can view datasets with the web version of evenor at qua.qualifiedself.org
```

Attachments
Attachments: add any additional information about the project that may help us to gain more insight into the proposed effort, for instance a more detailed task description, a justification of costs or relevant endorsements. Attachments should only contain background information, please make sure that the proposal without attachments is self-contained and concise. Don't waste too much time on this. Really.
Accepted formats for attachments are:
HTML, PDF, OpenDocument Format and plain text files.
(The total size of attachments must not exceed 50 MB)






