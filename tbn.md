Next deadline: April 1st 2024 12:00 CET (noon)

This form can be used to request support for your project. Note that releasing software, hardware and content under libre/open licenses, and the application of open standards where possible are transversal requirements for all. From our end we will provide a transparent and efficient selection process. Please check the (call-specific) guide for applicants for the call you are interested in.

Please also don't forget to look into our privacy statement about how we deal with your information (should be a pleasant surprise, actually — we really care about your privacy. Throughout the years we've funded the development of quite some widespread privacy enhancing technologies ourselves).

A practical point: we recommend to prepare longer answers offline, in case something goes wrong with your browser session. It is a light weight procedure, please don't wait until the last hour before the deadline before submitting — deadlines are hard.

Please select a call
In the list of current calls below, please indicate the call topic you are responding to. Note that some larger funds (like NGI0 Core and NGI0 Commons Fund) (part of the Next Generation Internet initiative) will have some special scope or conditions. You'd better have a look at them before you submit a proposal. If in doubt, submit to our open call and mention in the application that you are okay with us allocating your proposal to the most suitable fund.
Thematic call  NGI Zero Commons Fund

Contact information
Your name (max. 100 characters)      olivier
Email address                        olivier
Phone number                         olivier
Organisation (max. 100 characters)   Norcivilian Labs (association, not incorporated)
Country                              France

General project information
Project name (max. 100 characters)   evenor, csvs
Website / wiki                       https://gitlab.com/norcivilian-labs/evenor

Please be short and to the point in your answers; focus primarily on the what and how, not so much on the why. Add longer descriptions as attachments (see below). If English isn't your first language, don't worry — our reviewers don't care about spelling errors, only about great ideas. We apologise for the inconvenience of having to submit in English. On the up side, you can be as technical as you need to be (but you don't have to). Do stay concrete. Use plain text in your reply only, if you need any HTML to make your point please include this as attachment.

Abstract: Can you explain the whole project and its expected outcome(s).

1200 characters
```!
sql database management systems by design require corporate control, and subvert a fight for efficiency into a competition for feature crawl. data must be open for the rest of infrastructure to become open as well. the csvs data standard is radically open and imperatively transparent. https://csvs-format.docs.norcivilianlabs.org csvs keeps data in a plain text value store version controlled with git or pijul, and distributes binary media over synchronization protocols like Git Large File Storage and git-annex.
evenor is GPL-licensed graphical client for csvs, released for the web, desktop and all mobile devices. an evenor user can create datasets of text and media files, share the data over git hosting services, and collaborate with other editors. We help users rescue data from proprietary media services, concentrate content in local and self-hosted archives, and publish subsets of user content back to social networks, all in compliance with the "own your data" principle.
Norcivilian Labs conforms to the Standard for Public Code and keeps team communication public to promote accountability and value alignment.
```

Have you been involved with projects or organisations relevant to this project before? And if so, can you tell us a bit about your contributions?

(Optional) This can help us determine if you are the right person to undertake this effort

```!
Associates of Norcivilian Labs are the core developers of csvs and evenor, our mission is to prove the concept of an entire ecosystem build around the csvs standard. We publish specifications for the data format, the client libraries and the graphic applications, and provide reference implementations for each solution.
```

Requested support
Requested Amount between 5000 and 50000 (in Euro) 6000

Explain what the requested budget will be used for?

Does the project have other funding sources, both past and present?
(If you want, you can in addition attach a budget at the bottom of the form)

Explain costs for hardware, human labor (including rates used), travel cost to technical meetings, etc.

```!
budget will help us reach development milestones. 
200h total, issue breakdown below 

team:
Olivier Fantazor, software engineer, France $30/h
Nasim Abdulloev, software engineer, Republic of Armenia, $30/h https://www.linkedin.com/in/nasim-mora-5442912b7
fetsorn, Republic of Georgia, https://linkedin.com/in/fetsorn coordinator, software engineer, no pay

upcoming roadmap:
csvs-rs 50h
 - scaffold according to csvs-lib specification 10h
 - red-black tree search 20h
 - single-run record update with Boyer-Moore 20h
csvs-sql 100h
 - draft specification for structured query language over csvs 20h
 - SELECT, LIMIT, AS, DISTINCT, ORDER BY 10h
 - WHERE, AND, OR 10h
 - UPDATE, INSERT 10h
 - ALTER, CREATE TABLE 10h
 - COUNT, MIN, MAX, AVERAGE, SUM, GROUP BY 20h
 - INNER JOIN, LEFT JOIN, RIGHT JOIN 20h
evenor 40h
 - [X] list overview like Apple Notes (likely be done before grant)
 - [X] search with fuzzy matching like GitHub (likely be done before grant)
 - [X] graph editor for event-based GEDCOM schema (likely be done before grant)
 - view overview as record, scroll like Tinder 10h
 - view record as overview, enter like Inception 10h
 - bring UI in accordance with accessibility guidelines 20h
 
$100 for Apple developer fee to publish the existing Tauri mobile application to AppStore.
$25 for Google developer fee to publish the existing Tauri mobile application to Google Play.

Project never had other funding sources. No travel planned.
We do not purchase hardware. We host releases and run CI/CD pipeline at Gitlab (Ukraine) at no cost.
```

Compare your own project with existing or historical efforts.

(e.g. what is new, more thorough or otherwise different)

```!
Most contemporary note-taking, social media and messaging software stores data in binary databases that are accessed by corporate maintainers and only release subsets of data when forced by GDRP. csvs prioritizes highly accessible and durable plain-text data storage, and offers an abundnant list of import and export options for data interchange.
csvs strikes a balance between human-readability and memory efficiency, and provides a more reliable cli than other plain-text data storage formats like recutils, NestedText.
evenor builds on Tauri that is more secure and accessible than Electron used in Notion. 
evenor's free and open source policy guarantees long-term reliability in contrast to proprietary projects that cut and add features in neglect of consumer rights, like onenote, Apple Notes.
evenor strives for powerful search capabilities and interaction with major social networks, promising a healing effect on the web community, in contrast to fediverse solutions that compound segregation.
```

What are significant technical challenges you expect to solve during the project, if any?)

(optional but recommended)

```!
 - csvs-rs must implement efficient search over csvs datasets, building on a library specification and existiing naive reference implementations for Shell and JavaScript.
 - csvs-rs must abstract the version control interface to lay the ground for support of the pijul system.
 - csvs-sql must provide a robust foundation for SQL porcelain over csvs that does not compromise on intermidiary binary storage formats and preserves the plain text storage as main source of truth.
 - evenor must provide an approachable user interface that competes with major personal information management software regardless of underlying data ownership benefits.
```

Describe the ecosystem of the project, and how you will engage with relevant actors and promote the outcomes?

(E.g. which actors will you involve? Who should run or deploy your solution to make it a success?)

```!
Existing user community takes releases from CI/CD pipeline from GitLab.
we are acquaintances with selfprivacy.org that can setup self-hosted Gitea instances so that users view datasets in the web version of evenor, example at https://qua.qualifiedself.org
```

Attachments
Attachments: add any additional information about the project that may help us to gain more insight into the proposed effort, for instance a more detailed task description, a justification of costs or relevant endorsements. Attachments should only contain background information, please make sure that the proposal without attachments is self-contained and concise. Don't waste too much time on this. Really.
Accepted formats for attachments are:
HTML, PDF, OpenDocument Format and plain text files.
(The total size of attachments must not exceed 50 MB)







